/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.shapeframe;

/**
 *
 * @author AdMiN
 */
public class Trangle extends Shape {
    private double base;
    private double height;
    public Trangle(double base, double height) {
        super("Triangle");
        this.base = base;
        this.height = height;
    }
    public double getBase() {
        return base;
    }
    public void setBase(double base) {
        this.base = base;
    }
    public double getHeight() {
        return height;
    }
    public void setHeight(double height) {
        this.height = height;
    }
    @Override
    public double calArea() {
        return 0.5 * base * height;
    }
    @Override
    public double calPerimeter() {
        return base * 3;
    }

}
