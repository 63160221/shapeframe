/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.shapeframe;

/**
 *
 * @author AdMiN
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


    public class TrangleFrame extends JFrame {

    JLabel lblBase;
    JLabel lblHeight;
    JTextField txtBase;
    JTextField txtHeight;
    JButton btnCalArea;
    JLabel lblResult;

    public TrangleFrame() {
        super("Triangle");
        this.setSize(500, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblBase = new JLabel("Base: ");
        lblBase.setSize(50, 20);
        lblBase.setLocation(5, 5);
        this.add(lblBase);

        txtBase = new JTextField();
        txtBase.setSize(70, 20);
        txtBase.setLocation(60, 5);
        this.add(txtBase);

        lblHeight = new JLabel("Height: ");
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(5, 30);
        this.add(lblHeight);

        txtHeight = new JTextField();
        txtHeight.setSize(70, 20);
        txtHeight.setLocation(60, 30);
        this.add(txtHeight);

        btnCalArea = new JButton("Calculate");
        btnCalArea.setSize(100, 30);
        btnCalArea.setLocation(60, 60);
        this.add(btnCalArea);

        lblResult = new JLabel("Triangle: Base = ?? Height = ?? Area = ?? Perimeter = ??");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 100);
        this.add(lblResult);

        btnCalArea.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtBase.getText();
                    String strHeight = txtHeight.getText();
                    double base = Double.parseDouble(strBase);
                    double height = Double.parseDouble(strHeight);
                    Trangle triangle = new Trangle(base, height);
                    lblResult.setText("Triangle: Base = " + String.format("%.2f", base)
                            + " Height = " + String.format("%.2f", height)
                            + " Area = " + String.format("%.2f", triangle.calArea())
                            + " Perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TrangleFrame.this, "Error: Please input number!",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtHeight.setText("");
                    txtBase.requestFocus();
                }
            }
        });
    }

    public static void main(String[] args) {
        TrangleFrame frame = new TrangleFrame();
        frame.setVisible(true);
    }

}
