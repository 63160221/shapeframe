/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.shapeframe;

/**
 *
 * @author AdMiN
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Retangleframe extends JFrame {

    JLabel lblWidht;
    JLabel lblHeight;
    JTextField txtWidht;
    JTextField txtHeight;
    JButton btnCalArea;
    JLabel lblResult;

    public Retangleframe() {
        super("Rectangle");
        this.setSize(600, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblWidht = new JLabel("Widht: ");
        lblWidht.setSize(50, 20);
        lblWidht.setLocation(5, 5);
        this.add(lblWidht);

        txtWidht = new JTextField();
        txtWidht.setSize(70, 20);
        txtWidht.setLocation(60, 5);
        this.add(txtWidht);

        lblHeight = new JLabel("Height: ");
        lblHeight.setSize(70, 20);
        lblHeight.setLocation(5, 30);
        this.add(lblHeight);

        txtHeight = new JTextField();
        txtHeight.setSize(70, 20);
        txtHeight.setLocation(60, 30);
        this.add(txtHeight);

        btnCalArea = new JButton("Calculate");
        btnCalArea.setSize(100, 30);
        btnCalArea.setLocation(60, 60);
        this.add(btnCalArea);

        lblResult = new JLabel("Rectangle: Widht = ?? Height = ?? Area = ?? Perimeter = ??");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 100);
        this.add(lblResult);

        btnCalArea.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWidht = txtWidht.getText();
                    String strHeight = txtHeight.getText();
                    double widht = Double.parseDouble(strWidht);
                    double height = Double.parseDouble(strHeight);
                    Retangle rectangle = new Retangle(widht, height);
                    lblResult.setText("Rectangle: Widht = " + String.format("%.2f", widht)
                            + " Height = " + String.format("%.2f", height)
                            + " Area = " + String.format("%.2f", rectangle.calArea())
                            + " Perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(Retangleframe.this, "Error: Please input number!",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtWidht.setText("");
                    txtHeight.setText("");
                    txtWidht.requestFocus();
                }
            }
        });
    }

    public static void main(String[] args) {
        Retangleframe frame = new Retangleframe();
        frame.setVisible(true);
    }

}
