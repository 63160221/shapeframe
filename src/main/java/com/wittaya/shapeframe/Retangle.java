/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.shapeframe;

/**
 *
 * @author AdMiN
 */
public class Retangle extends Shape {
    private double widht;
    private double height;
    public Retangle(double widht, double height) {
        super("Rectangle");
        this.widht = widht;
        this.height = height;
    }
    public double getWidht() {
        return widht;
    }
    public void setWidht(double widht) {
        this.widht = widht;
    }
    public double getHeight() {
        return height;
    }
    public void setHeight(double height) {
        this.height = height;
    }
    @Override
    public double calArea() {
        return widht*height;
    }
    @Override
    public double calPerimeter() {
        return (widht*2)+(height*2);
    }
}
