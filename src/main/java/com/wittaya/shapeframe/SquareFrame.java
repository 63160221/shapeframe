/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.shapeframe;

/**
 *
 * @author AdMiN
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
public class SquareFrame extends JFrame{
     JLabel lblSide;
    JTextField txtSide;
    JButton btnCalArea;
    JLabel lblResult;
public SquareFrame(){
        super("Square");
        this.setSize(500, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        lblSide = new JLabel("Side: ", JLabel.TRAILING);
        lblSide.setSize(50, 20);
        lblSide.setLocation(5, 5);
        this.add(lblSide);
        
        txtSide = new JTextField();
        txtSide.setSize(70, 20);
        txtSide.setLocation(60, 5);
        this.add(txtSide);
        
        btnCalArea = new JButton("Calculate");
        btnCalArea.setSize(100, 30);
        btnCalArea.setLocation(60, 40);
        this.add(btnCalArea);
       
        lblResult = new JLabel("Square: Side = ?? Area = ?? Perimeter = ??");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 80);
        this.add(lblResult);

        btnCalArea.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strSide = txtSide.getText();
                double side = Double.parseDouble(strSide);
                Square square = new Square(side);
                lblResult.setText("Square: Side = " + String.format("%.2f", side)
                        + " Area = " + String.format("%.2f", square.calArea())
                        + " Perimeter = " + String.format("%.2f", square.calPerimeter()));
                } catch (Exception ex){
                    JOptionPane.showMessageDialog(SquareFrame.this, "Error: Please input number!"
                            , "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtSide.requestFocus();
                }
            }
        });
    }
public static void main(String[] args) {
        SquareFrame frame =  new SquareFrame();
        frame.setVisible(true);
     
    }

  
}
